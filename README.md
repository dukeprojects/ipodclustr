<div align="center">
   <img width="256" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/logo.png" alt="iPodClustr Logo">
</div>

<div align="center">
    ![Swift](https://img.shields.io/badge/Swift-5.1-orange.svg?style=flat)
    ![Xcode](https://img.shields.io/badge/Xcode-11.0-blue.svg?style=flat)
    ![version](https://img.shields.io/badge/version-1.0.0%20build%201-blue.svg?style=flat)
</div>

<div align="center">
    <h1>iPodClustr</h1>
</div>

**iPodClustr** is an app to make use of a cluster of iPods to achieve miscellaneous tasks. The goal is to first find out a way to establish a network connection between multiple iPods, presumably 8 (2x4) to 12 (2x6), and then combine them together to form a larger screen. By selecting a set of images from the master device, the combined screen can hence display the images separately on each iPod's screen, which create some interesting effects.

The app is developed by Ting Chen in *Duke's Center for Mobile Development* (CMD). The primary goal for the app is to make use of the outdated iPod touch devices in CMD and show off our technical skills, demonstrate posters of projects delivered by CMD, as well serve as an innovative and customizable electronic bulletin board to hang on the front door of CMD.

A quick demo video on 👉[Youtube](https://www.youtube.com/watch?v=uOLu2c8BNTE).

## Idea and Architecture

Please refer to [ideas](https://gitlab.oit.duke.edu/tc233/common_host/blob/master/Degree-Project-Ideas/ipodclustr.md) about the inspiration and initial design of the app.

In general, the app is designed to be an `ad-hoc` network client, with one master and several peers. The app establishes a network between iPods, and dispatch miscellaneous jobs to each of them.

## Features

<div>
    <img style="float: right" align="right" width="240" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/Degree-Project-Ideas/images/master-slave-network.png" title="master-slave-network" >
</div>

iPodClustr app has a few main features:

- [x] Master side:
  - Load images from master device's album and add them into a carousel display
  - Crop each image according to the designated dimensions. E.g., when there are 8 receiving iPods, the master will crop the image to exactly match their combined resolution and aspect ratio.
  - Compress and send image chunks to iPods
  - Freely add/remove images from the carousel queue
  - Static display mode: display one image without timeout
  - Transition animations
- [x] Receivers/peers side
  - Connect to master device
  - Receive images from master and display
- [x] ...More to be discovered in the app.

## Example

<div align="center">
    <img src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/videos/demo.gif" alt="Slides">
</div>

Above is a simple sample of iPodClustr with different transition style, in auto-play mode. As you can see, it is running automatically when the task queue is created, and will loop infinitely until master sends a termination signal or master quit the network.

<div align="center">
    <img src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/videos/sync.gif" alt="synchronized">
</div>

With the great handling of network traffic, the app has a rather "swoosh" synchronization between master and peers. In the animation above, when master return to the main menu, all the peers will follow without delay.

||
|:---------------:|
| `Master workflow demo` | 
|![Workflow](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/videos/workflow.mov)|
|iPodClustr has two sides integrated in one app. Above demonstrate the master side UX. When the grid/cluster dimensions are set up, user can add images to the dispatch queue at master side, and wait for peers to connect to the master. When designated amount of peers are connected (in this video only 1 to save some time), the master will stop broadcast for connection and is ready to dispatch images to its peers.|

## Designs and highlights

|||||
|:---------------:|:---------------:|:---------------:|:---------------:|
| `connect` | `animation` | `carousel` | `crop` |
|![connect](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/connect.png)|![animation](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/animation.png)|![carousel](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/carousel.png)|![crop](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/crop.png)|

Above are the main features at master side. On receiving peers side, it is basically listening to the signals from master, and either display the image or do other pre-defined actions.

- Establish an [ad-hoc](https://en.wikipedia.org/wiki/Wireless_ad_hoc_network) network between devices

    That is to say, this network does not rely on any back-end server or centralized endpoint. A central wireless access point (such as a wireless router) is optional, but not required. The network can run on local network, peer-to-peer WiFi or even Bluetooth (slow).

    By leveraging the power of Apple's `MultipeerConnectivity` framework, sockets between devices are abstracted to higher level `mcSession` objects. To build a master-slaves model of network, the master device will need to wait each peer to join the network in their alignment order - from upper left to bottom right. After designated amount of peers join the session, the broadcast/advertising for other peers stops, and the master is ready to start data sharing.

    Since MultipeerConnectivity framework sets an hard limit of at most 8 devices in a same mcSession, the master need to maintain multiple sessions to enable a cluster of more iPods. For 12 devices, I use 2 sessions to make it possible.

    Also, as the master greatly relies on the order/indices of peers to send chunked image pieces to correct position, it is not possible to maintain the network after master quits. Therefore, as soon as the peers cannot detect master's existence, they will consequently end the session.

    Finally, to reduce the network traffic, caching strategy is applied at both side. More will be covered below.

- Image chunking

    Multiple steps required for sharing image from master to peers.

    - Pick an image from master's album

        This can be easily achieved with Apple's native APIs for album access.

    - The master device loads an image, crop and chunk it and then send to all iPods.

        Since the dimensions of the cluster/grid of iPods are already defined, we can have the correct aspect ratio and combined resolution of the whole screen matrix. Therefore, we can crop the images according to the ratio to optimally fit the stitched screen resolution, and compress the image chunks if they have higher resolution to save memory space.

        As there are black borders around each iPod, two method of chunking is provided. One is to ignore the black borders, only consider the screen size and don't care about the dislocation due to the borders. Another is to treat the borders as a part of the screen matrix, and count them so that there won't be faults between devices.

        Therefore, a displayDevice object is designed to deal with all kinds of device dimensions and resolution.

```swift
struct displayDevice {
    
    /// resolution of device, width and height
    var resolution = (640, 1136)  // the default resolution of an iPod touch 6
    
    var physicalDimensions = (585, 1235)  // actual size is 58.57mm x 123.40mm, but this pair has better hcf of 65
    var horizontalBorderWidth = 43  // actual width is 4.325 mm
    var verticalBorderWidth = 174  // actual width is 17.405 mm
    
    init() {
    }
    
    init(resolution: (Int, Int)) {
        self.resolution = resolution
    }
    
    init(physicalDimensions: (Int, Int)) {
        self.physicalDimensions = physicalDimensions
    }
    
    /// get crop box aspect ratio with only the screen areas
    ///
    /// - Parameters:
    ///   - rows: input parameter from settings page, the rows count of the device matrix
    ///   - columns: input parameter from settings page, the columns count of the device matrix
    /// - Returns: a tuple of width:height, e.g. 9:16
    func getCropAspectRatioScreenOnly(_ rows: Int, _ columns: Int) -> (Int, Int) {
        let width = columns * resolution.0
        let height = rows * resolution.1
        let hcf = self.Gcd(a1: width, a2: height)
        return (width / hcf, height / hcf)
    }
    
    func getCropAspectRatioWithFrame(_ rows: Int, _ columns: Int) -> (Int, Int) {
        let width = columns * physicalDimensions.0
        let height = rows * physicalDimensions.1
        let hcf = self.Gcd(a1: width, a2: height)
        return (width / hcf, height / hcf)
    }
    
    func getScreenRectFromFrameRect(_ frameRect: CGRect) -> CGRect {
        let rectWidth = frameRect.width
        let rectHeight = frameRect.height
        // screen width and height in 1e-4 m, or 0.1mm
        let screenWidth = physicalDimensions.0 - horizontalBorderWidth - horizontalBorderWidth
        let screenHieght = physicalDimensions.1 - verticalBorderWidth - verticalBorderWidth
        
        // origin point in pixels, assuming all the rectangles are not tilted
        let x = CGFloat(horizontalBorderWidth) / CGFloat(physicalDimensions.0) * rectWidth + frameRect.minX
        let y = CGFloat(verticalBorderWidth) / CGFloat(physicalDimensions.1) * rectHeight + frameRect.minY
        
        // screen width and height in pixels
        let width = CGFloat(screenWidth) / CGFloat(physicalDimensions.0) * rectWidth
        let height = CGFloat(screenHieght) / CGFloat(physicalDimensions.1) * rectHeight
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    /// calculate highest common factor of two integers with 'greatest common divisor'/'Stein's algorithm'
    ///
    /// - Parameters:
    ///   - a1: first integer
    ///   - a2: second integer
    /// - Returns: hcf number
    private func Gcd(a1: Int, a2: Int) -> Int {
        var a = a1
        var b = a2
        while(b != 0) {
            let r = b
            b = a % b
            a = r
        }
        return a
    }
}

```
    
- Crop

    Also, greatest common divisor algorithm is implemented, to get the smallest mutually prime values of the crop box, so that the cropping UI won't be slow when calculating the pixel counts. 

    Using [CropViewController](https://github.com/TimOliver/TOCropViewController), cropping UI is consistent with iOS native cropping UI.

- Storage

    Image data are mostly stored in memory to ensure the fastest accessing speed. Later improvements might involve saving image chunks to disk or upload to static file servers to cut off memory consumption.

- Sync all slave devices when network is established

    By sending a struct with master device's timestamp and the time to start the action together with the data/signal, the clients can decide when to invoke the action in a near future. Then the difference between itself and master's clock is saved in the memory, until another sync signal is received to re-calibrate their times.

- Cache and synchronization

    - Cache

        While sending image data might be a heavier task for an ad-hoc network without optimization, adding cache to both master and peer side is a good shortcut to improve service quality.

        In my implementation, I cached image chunks at master side as well as peer side. Therefore, when master share the image chunk data to a peer for the first time, the peer will cache that data with a unique id indicating the related image. After that, the master will only send the id to the peers, and peer will load from cache in memory. Therefore, the transmission of huge binary data is avoided, and the performance is greatly improved after the first loop of the slides.

    - Synchronization

        Each peer synchronizes with the master every time they receive any type of signals. Therefore, the synchronization of the network is guaranteed and minimal delays ensured except the first loop binary data transmission.

        In order to achieve the synchronization, a better memory management scheme is enforced. It's the first time that I dive into ARC and used `[unowned self]` keyword to avoid retain cycle. Whenever the session stops and the master class need to be deinitialized, using this keyword will allow dispatched asynchronous tasks not to block the deinitialization of the master, and those async tasks in the future won't be executed since self is nil-ed.

- Socket programming/application on iOS devices

### Additional Highlights

- Fairly complex, no existing app or source code does this
- Can learn many new things network programming on mobile platform
- Show off our skills - the look is fancy and attractive
- Learn about distributed system in another sense
- Many options to design and implement
- Devise a multi device synchronization mechanism

![links](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/Degree-Project-Ideas/images/links.jpg)

Once I saw there is a TV wall in Duke Links Library. It seems that joint screens are already a mature technology and it is a bit trivial to *re-invent the wheel*.

My implementation might be naive and simple, but still practical as a hobby project.

## Look and Feel

||
|:---------------:|
| `devil` |
|![devil](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/devil.jpg)|
|`chapel` |
![chapel](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/chapel.jpg)|
| `hudson` |
|![hudson](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/iPodClustr/screenshots/hudson.jpg)|

## Contributing

Contributions are very welcome 🙌.

Fixing any [issues](https://gitlab.oit.duke.edu/tc233/ipodclustr/issues?scope=all&state=all) in the list can be a good start!

## Credits

- The template for this description is taken from [SwiftKit](https://github.com/SvenTiigi/SwiftKit/blob/master/Template/README.md).

- [Eureka](https://github.com/xmartlabs/Eureka) for form generator

- [CropViewController](https://github.com/TimOliver/TOCropViewController) for image cropping UI

- [Swinflate](https://github.com/VladIacobIonut/Swinflate) for image carousel

## License

```
iPodClustr app
Copyright © Center for Mobile Development 2019
All rights reserved. 
```

#### last revision: 190724 - Author - Ting Chen

---

## Roadmap

1. establish a test network on 3+1 devices
2. try image chunking with CoreImage framework. If it does not perform well, write C program to deal with image matrix
3. design the prototype of this app, and test its stability
4. UI improvements as well as potato game implementation
5. final tests

## progress notes

Make sure to add "master" to device name in settings for master device.

Right now the design ignores multi-slave synchronization, and all the queue, loop and delay are handled by the master device.

The master device cannot quit the session. When the master quits, all the slave peers will subsequently quit. And it does not make sense to let master send out invitation to slaves and join back to sessions.

---

[Original issue](https://gitlab.oit.duke.edu/MobileCenter/task_queue/issues/8)

