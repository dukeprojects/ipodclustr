//
//  MasterSettingsForms.swift
//  ipodclustr
//
//  Created by ECE564 on 7/14/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import Eureka

class MasterSettingsForms: FormViewController {
    
    var onApplyCallback : (([String: Any?]) -> Void)?
    var onCancelCallback : (() -> Void)?
    
    var emptyValues: [String: Any?] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section("Dimensions")
            <<< IntRow(){ row in
                row.title = "Rows (Max 2)"
                row.add(rule: RuleGreaterThan(min: 0, msg: "Must greater than 0", id: nil))
                row.add(rule: RuleSmallerOrEqualThan(max: 2, msg: "At most 2 rows supported", id: nil))
                row.validationOptions = .validatesOnBlur
                row.tag = "rows"
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        debugAlert(VC: self, title: "Error!", msg: row.validationErrors[0].msg, completion: {
                            cell.titleLabel?.textColor = .red
                        })
                        row.value = nil
                    }
            }
            <<< IntRow(){ row in
                row.title = "Columns (Max 6)"
                row.add(rule: RuleGreaterThan(min: 0, msg: "Must greater than 0", id: nil))
                row.add(rule: RuleSmallerOrEqualThan(max: 6, msg: "At most 6 columns supported", id: nil))
                row.validationOptions = .validatesOnBlur
                row.tag = "columns"
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        debugAlert(VC: self, title: "Error!", msg: row.validationErrors[0].msg, completion: {
                            cell.titleLabel?.textColor = .red
                        })
                        row.value = nil
                    }
            }
            <<< CheckRow(){ row in
                row.title = "First row flip"
                row.tag = "flip"
            }
            <<< CheckRow(){ row in
                row.title = "Crop device frame border"
                row.tag = "cropborder"
            }
            +++ Section("")
            <<< ButtonRow() {
                $0.title = "Cancel"
                }.onCellSelection { [weak self] (cell, row) in
                    self?.dismiss(animated: true, completion: {
                        self?.form.setValues(self!.emptyValues)
                        self?.onCancelCallback?()
                    })
            }
            <<< ButtonRow() {
                $0.title = "Apply"
                }.onCellSelection { [weak self] (cell, row) in
                    self?.dismiss(animated: true, completion: {
                        self?.onApplyCallback?(self?.form.values() ?? [:])  // inform
                })
            }
        // preserve the initial empty values to reset to all nil
        self.emptyValues = form.values()
    }
}
