//
//  LogPage.swift
//  ipodclustr
//
//  Created by ECE564 on 7/18/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit
import os

class LogPage: UIViewController {
    
    
    @IBOutlet weak var debugTextView: UITextView!
    
    var debugText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        os_log("ℹ️ Log page loaded. This page is used for checking network loggings", log: .default, type: .info)
        
        self.debugTextView.text = self.debugText
    }
    
    deinit {
        os_log("✅ log page deinited", log: .default, type: .debug)
    }
}
