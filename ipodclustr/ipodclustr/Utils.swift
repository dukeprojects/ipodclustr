//
//  Utils.swift
//  ipodclustr
//
//  Created by ECE564 on 7/14/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit
import CoreGraphics

/// debug alert for debugging purposes
///
/// - Parameters:
///   - VC: the VC to display the alert on
///   - title: title text
///   - msg: message for the alert
func debugAlert(VC: UIViewController, title: String, msg: String) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okAction)
    VC.present(alert, animated: true, completion: nil)
    return
}

/// debug alert box with completion closure
///
/// - Parameters:
///   - VC: the ViewController to present on
///   - title: alert title
///   - msg: alert message
///   - completion: alert completion handler
func debugAlert(VC: UIViewController, title: String, msg: String, completion: @escaping () -> Void) {
    DispatchQueue.main.async {  // Make sure you're on the main thread here, so no warnings and exceptions will be thrown
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion()
        })
        alert.addAction(okAction)
        VC.present(alert, animated: true, completion: nil)
        return
    }
}

extension CGSize {
    func resized(withMaximum side: CGFloat) -> CGSize {
        if side < 1 {  // error, just abort
            return self
        }
        let longerSide = self.height > self.width ? self.height : self.width
        if longerSide < side {
            return self
        }
        else {
            let ratio = side / longerSide
            return CGSize(width: self.width * ratio, height: self.height * ratio)
        }
    }
    
    func resizeToMaxSquare() -> CGSize {
        let m = max(self.width, self.height)
        return CGSize(width: m, height: m)
    }
}

extension Data {
    
    init<T>(from value: T) {
        self = Swift.withUnsafeBytes(of: value) { Data($0) }
    }
    
    func to<T>(type: T.Type) -> T? where T: ExpressibleByIntegerLiteral {
        var value: T = 0
        guard count >= MemoryLayout.size(ofValue: value) else { return nil }
        _ = Swift.withUnsafeMutableBytes(of: &value, { copyBytes(to: $0)} )
        return value
    }
}

extension UIImage {
    
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    func crop(rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
    
    func orientationFlip() -> UIImage {
        var newOrientation = self.imageOrientation
        switch self.imageOrientation {
        case .up:
            newOrientation = .down
        case .down:
            newOrientation = .up
        case .left:
            newOrientation = .right
        case .right:
            newOrientation = .left
        case .upMirrored:
            newOrientation = .downMirrored
        case .downMirrored:
            newOrientation = .upMirrored
        case .leftMirrored:
            newOrientation = .rightMirrored
        case .rightMirrored:
            newOrientation = .leftMirrored
        @unknown default:
            newOrientation = self.imageOrientation
        }
        let image = UIImage(cgImage: self.cgImage!, scale: self.scale, orientation: newOrientation)
        return image
    }
}


var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onWindow: UIWindow, text: String) {
        let spinnerView = UIView.init(frame: onWindow.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        // the view with blurry background and text
        let progressHUD = ProgressHUD(text: text)
        progressHUD.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(progressHUD)
            onWindow.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        vSpinner?.removeFromSuperview()
        vSpinner = nil
    }
    
    func showSpinner(onView: UIView, text: String) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)

        // the view with blurry background and text
        let progressHUD = ProgressHUD(text: text)
        progressHUD.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(progressHUD)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
}

/// A view to display an indicator together with a text
class ProgressHUD: UIVisualEffectView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    
    init(text: String) {
        self.text = text
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            // MARK: - this is a bad design! Should not fix the dimensions in code. Instead use dimension constant in a separate file is a better practice.
            let width = superview.frame.size.width / 2 > 200 ? 200 : superview.frame.size.width / 2
            let height: CGFloat = 50.0
            self.frame = CGRect(x: superview.frame.width / 2 - width / 2,
                                y: superview.frame.height / 2 - height / 2,
                                width: width,
                                height: height)
            vibrancyView.frame = self.bounds
            
            let activityIndicatorSize: CGFloat = 40
            activityIndictor.frame = CGRect(x: 5,
                                            y: height / 2 - activityIndicatorSize / 2,
                                            width: activityIndicatorSize,
                                            height: activityIndicatorSize)
            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.textAlignment = NSTextAlignment.center
            label.frame = CGRect(x: activityIndicatorSize + 5,
                                 y: 0,
                                 width: width - activityIndicatorSize - 15,
                                 height: height)
            label.textColor = UIColor.darkText
            label.font = UIFont.boldSystemFont(ofSize: 16)
        }
    }
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
}

extension UIView {
    
    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.5, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.3
        }, completion: completion)
    }
    
    func rotate360Infinitely(duration: Double) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = Float.infinity
        self.layer.add(rotateAnimation, forKey: "rotateViewAnimation")
    }
}

extension String {
    
    /// Generates a `UIImage` instance from this string using a specified attributes and size.
    ///
    /// - Parameters:
    ///     - attributes: to draw this string with. Default is `nil`.
    ///     - size: of the image to return.
    /// - Returns: a `UIImage` instance from this string using a specified
    /// attributes and size, or `nil` if the operation fails.
    func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
        let size = size ?? (self as NSString).size(withAttributes: attributes)
        let newSize = size.resizeToMaxSquare()
        return UIGraphicsImageRenderer(size: newSize).image { _ in
            (self as NSString).draw(in: CGRect(origin: .zero, size: newSize),
                                    withAttributes: attributes)
        }
    }
    
}

extension UIColor {
    convenience init(valueRGB: UInt, alpha: CGFloat = 1.0) {
        self.init(
            red: CGFloat((valueRGB & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((valueRGB & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(valueRGB & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

