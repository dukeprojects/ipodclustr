//
//  Config.swift
//  ipodclustr
//
//  Created by ECE564 on 7/14/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit

let globalMasterSubstring: String = "master"
let globalServiceType = "ic-01"  // ipodclustr 01
let globalVideoIDs: [Int: URL] = [
    1000: Bundle.main.url(forResource: "0", withExtension: "mov")!,
    1001: Bundle.main.url(forResource: "1", withExtension: "mov")!,
    1002: Bundle.main.url(forResource: "2", withExtension: "mov")!,
    1003: Bundle.main.url(forResource: "3", withExtension: "mov")!,
    1004: Bundle.main.url(forResource: "4", withExtension: "mov")!,
    1005: Bundle.main.url(forResource: "5", withExtension: "mov")!,
    1006: Bundle.main.url(forResource: "6", withExtension: "mov")!,
    1007: Bundle.main.url(forResource: "7", withExtension: "mov")!
]

struct globalAutoIncremental {
    static var IntID: Int = 0
    
    static func getIntID() -> Int {
        self.IntID += 1
        return self.IntID
    }
}


struct displayDevice {
    
    /// resolution of device, width and height
    var resolution = (640, 1136)  // the default resolution of an iPod touch 6
    
    var physicalDimensions = (585, 1235)  // actual size is 58.57mm x 123.40mm, but this pair has better hcf of 65
    var horizontalBorderWidth = 43  // actual width is 4.325 mm
    var verticalBorderWidth = 174  // actual width is 17.405 mm
    
    init() {
    }
    
    init(resolution: (Int, Int)) {
        self.resolution = resolution
    }
    
    init(physicalDimensions: (Int, Int)) {
        self.physicalDimensions = physicalDimensions
    }
    
    /// get crop box aspect ratio with only the screen areas
    ///
    /// - Parameters:
    ///   - rows: input parameter from settings page, the rows count of the device matrix
    ///   - columns: input parameter from settings page, the columns count of the device matrix
    /// - Returns: a tuple of width:height, e.g. 9:16
    func getCropAspectRatioScreenOnly(_ rows: Int, _ columns: Int) -> (Int, Int) {
        let width = columns * resolution.0
        let height = rows * resolution.1
        let hcf = self.Gcd(a1: width, a2: height)
        return (width / hcf, height / hcf)
    }
    
    func getCropAspectRatioWithFrame(_ rows: Int, _ columns: Int) -> (Int, Int) {
        let width = columns * physicalDimensions.0
        let height = rows * physicalDimensions.1
        let hcf = self.Gcd(a1: width, a2: height)
        return (width / hcf, height / hcf)
    }
    
    func getScreenRectFromFrameRect(_ frameRect: CGRect) -> CGRect {
        let rectWidth = frameRect.width
        let rectHeight = frameRect.height
        // screen width and height in 1e-4 m, or 0.1mm
        let screenWidth = physicalDimensions.0 - horizontalBorderWidth - horizontalBorderWidth
        let screenHieght = physicalDimensions.1 - verticalBorderWidth - verticalBorderWidth
        
        // origin point in pixels, assuming all the rectangles are not tilted
        let x = CGFloat(horizontalBorderWidth) / CGFloat(physicalDimensions.0) * rectWidth + frameRect.minX
        let y = CGFloat(verticalBorderWidth) / CGFloat(physicalDimensions.1) * rectHeight + frameRect.minY
        
        // screen width and height in pixels
        let width = CGFloat(screenWidth) / CGFloat(physicalDimensions.0) * rectWidth
        let height = CGFloat(screenHieght) / CGFloat(physicalDimensions.1) * rectHeight
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    /// calculate highest common factor of two integers with 'greatest common divisor'/'Stein's algorithm'
    ///
    /// - Parameters:
    ///   - a1: first integer
    ///   - a2: second integer
    /// - Returns: hcf number
    private func Gcd(a1: Int, a2: Int) -> Int {
        var a = a1
        var b = a2
        while(b != 0) {
            let r = b
            b = a % b
            a = r
        }
        return a
    }
}
