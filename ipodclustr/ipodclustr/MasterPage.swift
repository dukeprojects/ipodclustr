//
//  MasterPage.swift
//  ipodclustr
//
//  Created by ECE564 on 6/30/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CropViewController
import os
import SafariServices

class MasterPage: UIViewController {
    
    @IBOutlet weak var queueScrollView: UIScrollView!
    var slideButton: UIButton!
    var hostButton : UIButton!
    
    var debugText: String = "⬇️Check logs below⬇️"
    
    /// recvd from menu page
    var rows: Int!
    var columns: Int!
    var firstRowFlip: Bool!
    var cropborder: Bool!
    let device = displayDevice()
    
    /// A temporary variable to pass the delay settings from settings delegate to cropView delegate
    var delay: Int = 8
    /// The original not-cropped images with their metadata, stored in a dict whose key is a unique auto-incremental key, to differentiate each image
    var imagesSet: [Int: ImageWithMeta] = [:]
    /// A reference to the ImagePickerForm object
    var imageSettings: ImagePickerForm!
    /// A queue of id's of added images; use the auto-incremental id to facilitate the deletion and speed up lookup of an image in the queue
    var imageTaskQueue: [Int] = []
    /// An array of DispatchWorkItem. When master decide to disconnect, first cancel all the work items in this array, then pop the viewController
    var imageTaskQueueItems: [DispatchWorkItem] = []
    
    /// A flag tells either the iamge queue/slides is looping
    var isLooping: Bool = false
    
    /// keep track of the order when peer join the network, so that the image order will be correct
    var peerIDOrderMatrix: [[MCPeerID]] = [[], []]
    var currentRow: Int = 0
    
    // currently we have 12 iPods, so at most 2 sessions (each session can hold at most 8 devices including master) are needed
    var peerID1: MCPeerID!
    var peerID2: MCPeerID?
    
    var mcSession1: MCSession!
    var mcSession2: MCSession?
    
    let sessionCount = 2
    
    var session1MaxPeerCout: Int!
    
    var mcAdvertiserAssistant1: MCAdvertiserAssistant!
    var mcAdvertiserAssistant2: MCAdvertiserAssistant?
    
    var currentAdvertiserAssistant: MCAdvertiserAssistant!
    var currentConnectedPeersCount: Int = 0
    
    // MARK: - Properties of the collection view UI
    private var cellHeight: Int!
    private var cellWidth: Int!
    private var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initButtons()
        initColletionView()
        
        peerID1 = MCPeerID(displayName: UIDevice.current.name + "-" + globalMasterSubstring)
        mcSession1 = MCSession(peer: peerID1, securityIdentity: nil, encryptionPreference: .required)
        mcSession1.delegate = self
        
        if sessionCount > 1 {
            peerID2 = MCPeerID(displayName: UIDevice.current.name + "-" + globalMasterSubstring)
            mcSession2 = MCSession(peer: peerID2!, securityIdentity: nil, encryptionPreference: .required)
            mcSession2!.delegate = self
        }
        
        self.mcAdvertiserAssistant1 = MCAdvertiserAssistant(serviceType: globalServiceType, discoveryInfo: nil, session: mcSession1)
        self.mcAdvertiserAssistant1.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LogPageSegue" {
            let logPage = segue.destination as! LogPage
            logPage.debugText = self.debugText
        }
    }
    
    @objc
    func sendButtonTapped(_ sender: UIButton) {
        if self.imagesSet.isEmpty {
            return
        }
        self.isLooping = !self.isLooping
        if self.isLooping {
            // sender.setTitle("stop slides", for: .normal)
            self.slideButton.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
            self.loopThroughImagesSet()
        }
        else {
            // sender.setTitle("start slides", for: .normal)
            self.slideButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            // black screen slaves, also reset scrollview offset
            self.sendID(Data(from: 0))  // set id of black as 0
            self.sendImage(img: #imageLiteral(resourceName: "black"))
            self.queueScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            if self.imagesSet.count > 0 {
                self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                for i in self.imagesSet.keys {
                    self.imagesSet[i]!.isSent = false  // reset flag, next time will be a fresh start
                }
            }
            for item in self.imageTaskQueueItems {
                item.cancel()
            }
            self.imageTaskQueue.removeAll()
            return
        }
    }
    
    @objc
    func debugTextButtonTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "LogPageSegue", sender: nil)
    }
    
    @objc
    func addImageButtonTapped(_ sender: Any) {
        imageSettings = ImagePickerForm()
        imageSettings.delegate = self
        self.present(imageSettings, animated: true)
        self.showSpinner(onView: self.view, text: "Processing...")
    }
    
    @objc
    func startHostingButtonTapped(_ sender: UIButton) {
        self.debugText.append("\n")
        self.debugText.append("debug info: started hosting!\n")
        self.startHosting(action: nil)
        self.hostButton.setImage(#imageLiteral(resourceName: "process-circle"), for: .normal)
        self.hostButton.rotate360Infinitely(duration: 3)
        self.hostButton.isUserInteractionEnabled = false
    }
    
    @objc
    func webButtonTapped(_ sender: UIButton) {
        let urlString = "https://www.google.com"
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            let safariViewController = SFSafariViewController(url: url)
            safariViewController.preferredControlTintColor = UIColor.init(named: "dukeBlue")
            self.present(safariViewController, animated: true)
        }
    }
    
    @objc
    func textButtonTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "Send Image", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Add your text here..."
            textField.keyboardType = .default
        }
        let sendAction = UIAlertAction(title: "Send", style: .default) { (_) in
            guard let text = alertController.textFields![0].text else {
                return
            }
            let textAttributes: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.foregroundColor: UIColor(valueRGB: 0x012169),
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 120)
            ]
            let image = text.image(withAttributes: textAttributes)
            let resultDict: [String: Any?] = [
                "image": image,
                "delay": 10
            ]
            self.onApply(result: resultDict)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc
    func videoButtonTapped(_ sender: UIButton) {
        // video should override any playing slides, and add to the top of subviews that overlap all other stuff.
        // video view will hide automatically after playback is over.
        
        let firstRowIDs = globalVideoIDs.keys.sorted()[0..<4]
        let secondRowIDs = globalVideoIDs.keys.sorted()[4..<8]
        let idArray: [[Data]] = [firstRowIDs.map {Data(from: $0)}, secondRowIDs.map {Data(from: $0)}]
        
        #warning("being hacky here!")
        self.sendCroppedImage(idArray)  // a random id that won't collide, specifically for a video clip
    }
    
    func startHosting(action: UIAlertAction?) {
        if let s2 = mcSession2 {
            self.mcAdvertiserAssistant2 = MCAdvertiserAssistant(serviceType: globalServiceType, discoveryInfo: nil, session: s2)
            self.mcAdvertiserAssistant2?.delegate = self
        }
        self.currentAdvertiserAssistant = self.mcAdvertiserAssistant1
        self.mcAdvertiserAssistant1.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            os_log("✅ Master view will pop from nav stack, first clean up tasks", log: .default, type: .debug)
            self.isLooping = false
            self.imageTaskQueue.removeAll()
            for item in self.imageTaskQueueItems {
                item.cancel()
            }
            self.imageTaskQueueItems.removeAll()
        }
    }
    
    deinit {
        os_log("✅ Master deinited", log: .default, type: .debug)
        self.mcAdvertiserAssistant1.stop()
        self.mcAdvertiserAssistant2?.stop()
    }
}


extension MasterPage: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func initColletionView() {
        let layout = SWInflateLayout()
        layout.isPagingEnabled = true
        layout.leftContentOffset = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 10)
        collectionView.register(BasicCollectionViewCell.self, forCellWithReuseIdentifier: "BasicCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false  // hide the scroll bar
        
        self.view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -100).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        
        cellWidth = Int(view.bounds.width - 60)
        cellHeight = cellWidth > 200 ? cellWidth : 200
    }
    
    // MARK: - CollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesSet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasicCell", for: indexPath) as? BasicCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let imgs = self.imagesSet.sorted(by: {$0.key < $1.key})
        cell.button.addTarget(self, action: #selector(collectionViewButtonTapped), for: .touchUpInside)
        cell.button.setImage(imgs[indexPath.row].value.originalImage, for: .normal)
        cell.button.tag = imgs[indexPath.row].key
        cell.label.text = "Image id: \(imgs[indexPath.row].key)"
        return cell
    }
    
    // MARK: - UICollectionViewLayoutDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellHeight)
    }
}


// MARK: - ImagePickerFormDelegate for obtaining image slide settings
extension MasterPage: ImagePickerFormDelegate {
    
    func onApply(result: [String : Any?]) {
        var aspectRatio = (1, 1)
        if self.cropborder {
            aspectRatio = self.device.getCropAspectRatioWithFrame(self.rows, self.columns)
        }
        else {
            aspectRatio = self.device.getCropAspectRatioScreenOnly(self.rows, self.columns)
        }
        if let delay: Int = result["delay"] as? Int {
            self.delay = delay
        }
        else {
            self.delay = 8
        }
        
        if let image: UIImage = result["image"] as? UIImage {
            let s = CGSize(width: aspectRatio.0, height: aspectRatio.1)
            let cropViewController = CropViewController(croppingStyle: .default, image: image)
            cropViewController.customAspectRatio = s
            cropViewController.aspectRatioPickerButtonHidden = true
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.delegate = self
            self.present(cropViewController, animated: true, completion: nil)
        }
        else {
            self.removeSpinner()
        }
    }
    
    func onCancel() {
        os_log("✅ Remove due to cancel", log: .default, type: .debug)
        self.removeSpinner()
    }
}

extension MasterPage: CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        let width = Int(cropRect.width) / self.columns
        let height = Int(cropRect.height) / self.rows
        
        var x = 0
        var y = 0
        
        var rowsImages: [Data] = []
        var croppedImageJPEGDataSet: [[Data]] = []
        
        for r in 0..<self.rows {
            y = r * height
            rowsImages.removeAll()
            for c in 0..<self.columns {
                x = c * width
                var rect = CGRect(x: x, y: y, width: width, height: height)
                if self.cropborder {
                    rect = self.device.getScreenRectFromFrameRect(rect)
                }
                var im = image.crop(rect: rect)
                
                // resize image pieces that are too large
                if Int(im.size.width) > self.device.resolution.0 {
                    im = im.resized(to: CGSize(width: self.device.resolution.0, height: self.device.resolution.1))
                }
                
                if r != 0 {
                    rowsImages.append(im.jpegData(compressionQuality: 0.85)!)
                }
                else {
                    if self.firstRowFlip {
                        rowsImages.append(im.orientationFlip().jpegData(compressionQuality: 0.85)!)
                    }
                    else {
                        rowsImages.append(im.jpegData(compressionQuality: 0.85)!)
                    }
                }
            }
            croppedImageJPEGDataSet.append(rowsImages)
        }
        
        let id = globalAutoIncremental.getIntID()
        
        let compressedSize = image.size.resized(withMaximum: 256)  // long side smaller than 256 px to save memory
        let compressedImage = image.resized(to: compressedSize)
        
        let imageMeta = ImageWithMeta(id: id, delay: self.delay, originalImage: compressedImage, croppedImageJPEGDataSet: croppedImageJPEGDataSet, isSent: false)
        self.imagesSet[imageMeta.id] = imageMeta
        
        self.dismiss(animated: true, completion: {
            self.collectionView.reloadData()
            self.removeSpinner()
        })
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        // do nothing and remove the spinner
        self.dismiss(animated: true, completion: {
            self.removeSpinner()
        })
    }
}


extension MasterPage: MCAdvertiserAssistantDelegate {
    
    func advertiserAssistantWillPresentInvitation(_ advertiserAssistant: MCAdvertiserAssistant) {
        advertiserAssistant.stop()
        os_log("✅ Will stop current MCAdvertiserAssistant until peer connected.", log: .default, type: .debug)
    }
    
    func advertiserAssistantDidDismissInvitation(_ advertiserAssistant: MCAdvertiserAssistant) {
        // just do nothing here, maybe resume UI update in future
    }
}

extension MasterPage {
    
    func initButtons() {
        self.initLogButton()
        
        let buttonWidth = 96
        let xButtonPadding = 12
        let yButtonPadding = 12
        let xOffset = 6 * (buttonWidth + xButtonPadding)
        
        self.initAddButton(xButtonPadding: xButtonPadding, yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        self.initStartHostingButton(xButtonPadding: xButtonPadding + (buttonWidth + xButtonPadding), yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        self.initSlidesButton(xButtonPadding: xButtonPadding + 2 * (buttonWidth + xButtonPadding), yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        self.initTextButton(xButtonPadding: xButtonPadding + 3 * (buttonWidth + xButtonPadding), yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        self.initWebButton(xButtonPadding: xButtonPadding + 4 * (buttonWidth + xButtonPadding), yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        self.initVideoButton(xButtonPadding: xButtonPadding + 5 * (buttonWidth + xButtonPadding), yButtonPadding: yButtonPadding, buttonWidth: buttonWidth)
        
        self.queueScrollView.contentSize = CGSize(width: CGFloat(xOffset), height: queueScrollView.frame.height)
    }
    
    func initLogButton() {
        let button = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(debugTextButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func initAddButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(addImageButtonTapped), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
        button.showsTouchWhenHighlighted = true
        self.queueScrollView.addSubview(button)
    }
    
    func initStartHostingButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(startHostingButtonTapped(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "process-circle copy"), for: .normal)
        button.showsTouchWhenHighlighted = true
        self.hostButton = button
        self.queueScrollView.addSubview(button)
    }
    
    func initSlidesButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(sendButtonTapped(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        button.showsTouchWhenHighlighted = true
        self.slideButton = button
        self.queueScrollView.addSubview(button)
    }
    
    func initWebButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(webButtonTapped(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "compass"), for: .normal)
        button.showsTouchWhenHighlighted = true
        self.queueScrollView.addSubview(button)
    }
    
    func initTextButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(textButtonTapped(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "text"), for: .normal)
        button.showsTouchWhenHighlighted = true
        self.queueScrollView.addSubview(button)
    }
    
    func initVideoButton(xButtonPadding: Int, yButtonPadding: Int, buttonWidth: Int) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: xButtonPadding, y: yButtonPadding, width: buttonWidth, height: buttonWidth)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(videoButtonTapped(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "video"), for: .normal)
        button.showsTouchWhenHighlighted = true
        // only allow for 2*4 situation
        if self.rows != 2 && self.columns != 4 {
            button.isEnabled = false
        }
        self.queueScrollView.addSubview(button)
    }
    
    @objc
    func collectionViewButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Please select an action", message: "For the image", preferredStyle: UIAlertController.Style.actionSheet)
        let deleteAction = UIAlertAction(title: "Delete from queue", style: .destructive, handler: { _ in
            var ind: Int = 0
            for img in self.imagesSet.sorted(by: {$0.key < $1.key}) {
                if img.value.id == sender.tag {
                    ind = img.value.id
                    break
                }
            }
            self.imagesSet.removeValue(forKey: ind)
            self.imageTaskQueue.removeAll(where: {ind == $0})
            self.collectionView.reloadData()
        })
        let overrideAction = UIAlertAction(title: "Send this image", style: .default, handler: { _ in
            self.sendImageOrID(self.imagesSet[sender.tag]!)
        })
        let sendOriginalImage = UIAlertAction(title: "Send repetitive image", style: .default, handler: { _ in
            self.sendID(Data(from: 9901))// a random id that won't collide
            self.sendImage(img: self.imagesSet[sender.tag]!.originalImage)
        })
        let sendWithDelay = UIAlertAction(title: "Send with delay", style: .default, handler: { _ in
            if self.imagesSet[sender.tag]!.isSent {
                let idData = Data(from: self.imagesSet[sender.tag]!.id)
                self.sendIDWithDelay(imageIDData: idData, delay: 0.5)
            }
            else {
                self.sendImageOrID(self.imagesSet[sender.tag]!)
            }
        })
        let sendBlackScroll = UIAlertAction(title: "Send black scroll", style: .default, handler: { _ in
            self.sendIDWithDelay(imageIDData: Data(from: 0), delay: 0.5)
            self.sendImageOrID(self.imagesSet[sender.tag]!)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(overrideAction)
        alert.addAction(sendOriginalImage)
        alert.addAction(sendWithDelay)
        alert.addAction(sendBlackScroll)
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = sender  // for iPad only
        self.present(alert, animated: true, completion: nil)
        return
    }
    
    
    func sendImage(img: UIImage) {
        if mcSession1.connectedPeers.count > 0 {
            if let imageData = img.pngData() {
                do {
                    try mcSession1.send(imageData, toPeers: mcSession1.connectedPeers, with: .reliable)
                }
                catch let error as NSError {
                    let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    present(ac, animated: true)
                }
            }
        }
        // only send when session2 exists
        if let mcSession2 = self.mcSession2 {
            if mcSession2.connectedPeers.count > 0 {
                if let imageData = img.pngData() {
                    do {
                        try mcSession2.send(imageData, toPeers: mcSession2.connectedPeers, with: .reliable)
                    }
                    catch let error as NSError {
                        let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        present(ac, animated: true)
                    }
                }
            }
        }
    }
    
    func sendImageOrID(_ imageWithMeta: ImageWithMeta) {
        let idData = Data(from: imageWithMeta.id)
        
        if imageWithMeta.isSent {
            self.sendID(idData)
        }
        else {
            self.sendID(idData)
            self.sendCroppedImage(imageWithMeta.croppedImageJPEGDataSet)
        }
    }
    
    private func sendID(_ imageIDData: Data) {
        if mcSession1.connectedPeers.count > 0 {
            do {
                try mcSession1.send(imageIDData, toPeers: mcSession1.connectedPeers, with: .reliable)  // maybe change to unreliable for better availabilty
            }
            catch let error as NSError {
                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                present(ac, animated: true)
            }
        }
        // only send when session2 exists
        if let mcSession2 = self.mcSession2 {
            if mcSession2.connectedPeers.count > 0 {
                do {
                    try mcSession2.send(imageIDData, toPeers: mcSession2.connectedPeers, with: .reliable)
                }
                catch let error as NSError {
                    let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    present(ac, animated: true)
                }
            }
        }
    }
    
    private func sendIDWithDelay(imageIDData: Data, delay: Double) {
        var delayTime = delay
        if mcSession1.connectedPeers.count > 0 {
            for peer in self.peerIDOrderMatrix[0] {
                DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime) {
                    do {
                        try self.mcSession1.send(imageIDData, toPeers: [peer], with: .reliable)
                    }
                    catch _ as NSError {
                        // do nothing with the error
                    }
                }
                delayTime += delay
            }
        }
        // only send when session2 exists
        if let mcSession2 = self.mcSession2 {
            if mcSession2.connectedPeers.count > 0 {
                for peer in self.peerIDOrderMatrix[1] {
                    DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime) {
                        do {
                            try mcSession2.send(imageIDData, toPeers: [peer], with: .reliable)
                        }
                        catch _ as NSError {
                            // do nothing with the error
                        }
                    }
                    delayTime += delay
                }
            }
        }
    }
    
    func sendCroppedImage(_ croppedImageJPEGDataSet: [[Data]]) {
        
        var row1Images: [Data] = []
        var row2Images: [Data] = []
        
        print(self.peerIDOrderMatrix)
        
        if croppedImageJPEGDataSet.count == 2 {
            row1Images = croppedImageJPEGDataSet[0]
            row2Images = croppedImageJPEGDataSet[1]
        }
        else if croppedImageJPEGDataSet.count == 1 {
            row1Images = croppedImageJPEGDataSet[0]
        }
        else {
            os_log("❌ No images in the cropped matrix. This should not happen", log: .default, type: .error)
            return
        }
        if row1Images.count > 0 {
            if mcSession1.connectedPeers.count > 0 {
                for (i, img) in row1Images.enumerated() {
                    if !mcSession1.connectedPeers.contains(self.peerIDOrderMatrix[0][i]) {
                        continue  // connection problem, just ignore
                    }
                    do {
                        try mcSession1.send(img, toPeers: [self.peerIDOrderMatrix[0][i]], with: .reliable)
                    }
                    catch let error as NSError {
                        let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        present(ac, animated: true)
                    }
                }
            }
        }
        if row2Images.count > 0 {
            // only send when session2 exists
            if let mcSession2 = self.mcSession2 {
                if mcSession2.connectedPeers.count > 0 {
                    for (i, img) in row2Images.enumerated() {
                        if !mcSession2.connectedPeers.contains(self.peerIDOrderMatrix[1][i]) {
                            continue  // connection problem, just ignore
                        }
                        do {
                            try mcSession2.send(img, toPeers: [self.peerIDOrderMatrix[1][i]], with: .reliable)
                        }
                        catch let error as NSError {
                            let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .default))
                            present(ac, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func loopThroughImagesSet() {
        // enqueue all the image indices
        for img in self.imagesSet.sorted(by: {$0.key < $1.key}) {
            self.imageTaskQueue.append(img.value.id)
        }
        
        var delayTime: Double = 0
        for idImgPair in self.imagesSet.sorted(by: {$0.key < $1.key}) {
            let dispatchItem: DispatchWorkItem = DispatchWorkItem { [unowned self] in self.taskQueuePopleft() }
            imageTaskQueueItems.append(dispatchItem)
            DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime, execute: dispatchItem)
            DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
                if let index = self.imagesSet.sorted(by: {$0.key < $1.key}).firstIndex(where: {$0.key == idImgPair.key}) {
                    self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            delayTime = delayTime + Double(idImgPair.value.delay)
        }
        
        let dispatchItemNewLoop: DispatchWorkItem = DispatchWorkItem { [unowned self] in
            os_log("✅ Slides are still looping!", log: .default, type: .debug)
            self.imageTaskQueueItems.removeAll()
            if self.isLooping {
                self.loopThroughImagesSet()
            }
        }
        imageTaskQueueItems.append(dispatchItemNewLoop)
        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime, execute: dispatchItemNewLoop)
        os_log("✅ one loop added!", log: .default, type: .debug)
    }
    
    
    func taskQueuePopleft() {
        if self.imageTaskQueue.isEmpty {
            return
        }
        
        let i = self.imageTaskQueue.removeFirst()
        
        if self.imagesSet[i] == nil {
            os_log("❌ taskqueue has problem! abort from current task", log: .default, type: .error)
            return
        }
        // self.sendCroppedImage(self.imagesSet[i]!.croppedImageJPEGDataSet)
        // replacement
        self.sendImageOrID(self.imagesSet[i]!)
        self.imagesSet[i]!.isSent = true
    }
    
}

extension MasterPage: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        if self.mcSession1.connectedPeers.count >= self.session1MaxPeerCout && self.currentAdvertiserAssistant !== self.mcAdvertiserAssistant2 {  // reach max count and not switched yet, compare memory address
            self.mcAdvertiserAssistant1.stop()
            self.mcAdvertiserAssistant2?.start()
            self.currentAdvertiserAssistant = self.mcAdvertiserAssistant2
        }
        switch state {
        case MCSessionState.connected:
            self.currentAdvertiserAssistant.start()
            self.peerIDOrderMatrix[self.currentRow].append(peerID)
            os_log("✅ Connected: %@", log: .default, type: .debug, peerID.displayName)
            DispatchQueue.global(qos: .utility).async { [unowned self] in
                self.debugText.append("Connected: \(peerID.displayName)\n")
                self.debugText.append("✅ debug info: currently session1 \(self.mcSession1.connectedPeers) count = \(self.mcSession1.connectedPeers.count) \n")
                self.debugText.append("✅ debug info: currently session2 \(String(describing: self.mcSession2?.connectedPeers))  count = \(String(describing: self.mcSession2?.connectedPeers.count)) \n")
            }
            self.currentConnectedPeersCount = self.mcSession1.connectedPeers.count + (self.mcSession2?.connectedPeers.count ?? 0)
            if rows * columns == self.currentConnectedPeersCount {
                DispatchQueue.main.async { [unowned self] in
                    self.hostButton.layer.removeAllAnimations()  // stop spinning
                }
            }
        case MCSessionState.connecting:
            os_log("✅ Connecting: %@", log: .default, type: .debug, peerID.displayName)
        case MCSessionState.notConnected:
            self.currentAdvertiserAssistant.start()
            os_log("✅ Not Connected: %@", log: .default, type: .debug, peerID.displayName)
        @unknown default:
            fatalError()
        }
        if self.mcSession1.connectedPeers.count >= self.session1MaxPeerCout {
            self.currentRow = 1
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        // do nothing here right now
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        // do nothing here right now
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        // do nothing here right now
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        // do nothing here right now
    }
    
}

