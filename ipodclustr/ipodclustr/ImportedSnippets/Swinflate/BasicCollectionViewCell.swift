//
//  BasicCollectionViewCell.swift
//  Example
//
//  Created by Vlad on 1/7/19.
//  Copyright © 2019 Vlad Iacob. All rights reserved.
//

import UIKit

class BasicCollectionViewCell: UICollectionViewCell {
    // MARK: - Properties
    
    let label = UILabel()
    var button = UIButton()

    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    
    private func setupUI() {
        
        contentView.addSubview(button)
        contentView.addSubview(label)
        contentView.contentMode = .scaleAspectFit
        contentView.clipsToBounds = true  // to clip the button image
        contentView.layer.cornerRadius = 15
        contentView.backgroundColor = UIColor.lightGray  // apple red
        
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowRadius = 20
        contentView.layer.shadowOpacity = 0.2
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        button.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0).isActive = true
        button.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        button.showsTouchWhenHighlighted = true
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.fill
        button.contentVerticalAlignment = UIControl.ContentVerticalAlignment.fill
        button.imageView?.contentMode = .scaleAspectFill
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20).isActive = true
        label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textColor = .white
    }
}
