//
//  deprecated.swift
//  ipodclustr
//
//  Created by ECE564 on 7/13/19.
//  Copyright © 2019 mobilecenter. All rights reserved.
//

import Foundation


//    func updateDiscoveryInfoDict() {
//        let count1 = self.mcSession1.connectedPeers.count
//        discoveryInfoDict["session1PeerCount"] = String(count1)
//        if let mc2 = self.mcSession2 {
//            let count2 = mc2.connectedPeers.count
//            discoveryInfoDict["session2PeerCount"] = String(count2)
//        }
//        self.textView.text.append("✅ \(discoveryInfoDict.description)\n")
//
//    }

//func sendImage(img: UIImage) {
//    if mcSession1.connectedPeers.count > 0 {
//        if let imageData = img.pngData() {
//            do {
//                try mcSession1.send(imageData, toPeers: mcSession1.connectedPeers, with: .reliable)
//            }
//            catch let error as NSError {
//                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .default))
//                present(ac, animated: true)
//            }
//        }
//    }
//    // only send when session2 exists
//    if let mcSession2 = self.mcSession2 {
//        if mcSession2.connectedPeers.count > 0 {
//            if let imageData = img.pngData() {
//                do {
//                    try mcSession2.send(imageData, toPeers: mcSession2.connectedPeers, with: .reliable)
//                }
//                catch let error as NSError {
//                    let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
//                    ac.addAction(UIAlertAction(title: "OK", style: .default))
//                    present(ac, animated: true)
//                }
//            }
//        }
//    }
//}

//@objc
//func addImageButtonTapped(_ sender: Any) {
//    let imageSettings = ImagePickerForm()
//    self.present(imageSettings, animated: true)
//    self.showSpinner(onView: self.view, text: "Processing...")
//    
//    imageSettings.onApplyCallback = { result in
//        var aspectRatio = (1, 1)
//        
//        if self.cropborder {
//            aspectRatio = self.device.getCropAspectRatioWithFrame(self.rows, self.columns)
//        }
//        else {
//            aspectRatio = self.device.getCropAspectRatioScreenOnly(self.rows, self.columns)
//        }
//        if let delay: Int = result["delay"] as? Int {
//            self.delay = delay
//        }
//        
//        if let image: UIImage = result["image"] as? UIImage {
//            let s = CGSize(width: aspectRatio.0, height: aspectRatio.1)
//            let cropViewController = CropViewController(croppingStyle: .default, image: image)
//            cropViewController.customAspectRatio = s
//            cropViewController.aspectRatioPickerButtonHidden = true
//            cropViewController.aspectRatioLockEnabled = true
//            cropViewController.delegate = self
//            self.present(cropViewController, animated: true, completion: nil)
//        }
//        else {
//            os_log("✅ No image get, abort", log: .default, type: .debug)
//            self.removeSpinner()
//        }
//    }
//    imageSettings.onCancelCallback = {
//        self.removeSpinner()
//        return
//    }
//    
//}

//            _ = Timer.scheduledTimer(withTimeInterval: delayTime, repeats: false) { timer in
//                self.taskQueuePopleft()
//            }

//            DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime) {
//                self.sendCroppedImage(img.croppedImageJPEGDataSet)
//            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
//                self.queueScrollView.setContentOffset(CGPoint(x: self.imageButtonsSet[i].frame.minX, y: 0), animated: true)
//                self.imageButtonsSet[i].alpha = 0.3
//                self.imageButtonsSet[i].fadeIn(TimeInterval(img.delay), delay: 0.5)
//            }

//        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + delayTime) {
//            self.sendImage(img: #imageLiteral(resourceName: "black"))
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
//            self.queueScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//        }

//        let testImagePath = Bundle.main.path(forResource: "avatar", ofType: "png")!
//        let image = UIImage(contentsOfFile: testImagePath)!
//        sendImage(img: image)
